package com.zkx.common.result;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

//自定义统一响应体
@Getter
@Setter
//使得 实体类在返回前端的时候忽略字段属性为null的字段，使其为null字段不显示。
@JsonSerialize(include= JsonSerialize.Inclusion.NON_NULL)
public class ResultVO<T> implements Serializable {
    //状态码
    private Integer code;
    //响应信息
    private String msg;
    //响应的具体数据
    private T data;
    //操作成功 状态码 1000
    private static final Integer successCode = 1000;
    //默认构造器 设置为成功时返回情况
    public ResultVO(){
        this.code = successCode;
        this.msg = "操作成功";
    }
    public ResultVO(T data){
        this();
        this.data = data;
    }

    public ResultVO(Integer code,String msg){
        this();
        this.code = code;
        this.msg = msg;
    }
    public ResultVO(Integer code,String msg,T data){
        this();
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
}
