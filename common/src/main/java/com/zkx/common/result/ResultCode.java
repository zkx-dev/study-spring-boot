package com.zkx.common.result;


//响应码枚举类
public enum ResultCode {


    //1000系列通用错误
    FAILED(1001,"接口错误"),
    VALIDATE_FAILED(1002,"参数校验错误"),
    ERROR(1003,"redis数据库中的信息出现异常操作"),

    //2000系列用户错误
    USER_NOT_EXIST(2000,"用户id不存在"),
    USER_LOGIN_FAIL(2001,"用户名或密码错误"),
    USER_NOT_LOGIN(2002,"用户还未登录，无法登出"),
    NO_PERMISSION(2003,"权限不足，请联系管理员"),
    ILLEGAL_LENGTH(2004,"用户名或密码长度不符合要求"),
    NOT_EMPTY(2005,"用户名或密码不能为空"),
    USER_ALREADY_LOGGED_IN(2006,"此用户已经登录"),
    USER_BORROWED_BOOK_NOT_RETURNED(2008,"用户借书未还，不能删除用户"),
    USER_NOT_BORROW_BOOK(2009,"用户没有借过这本书，还书失败"),
    USER_HAS_BORROW_BOOK(2010,"用户借阅这本书未还，借书失败"),
    USER_NOT_ROLE(2011,"用户没有分配角色"),
    USER_NOT_ACCESS(2012,"用户没有分配角色或者角色没有任何权限"),

    //3000系列 TOKEN错误
    TOKEN_ERROR(3001,"token验证失败"),
    TOKEN_EMPTY(3002,"该用户没有token信息"),
    TOKEN_ALGORITHM_INCONSISTENT(3003,"token算法不一致"),
    TOKEN_EXPIRED(3004,"token过期"),
    SIGNATURE_INVALID(3005,"无效签名"),



    //4000系列 图书错误
    BOOK_NAME_EMPTY(4001,"图书名不能为空"),
    BOOK_NAME_ILLEGAL(4002,"图书名长度不符合要求"),
    BOOK_QUANTITY_ILLEGAL(4003,"图书数量不符合要求"),
    BOOK_HAS_BORROWED(4004,"当前图书有人借阅，无法操作"),
    BOOK_NOT_EXIST(4005,"图书ID不存在"),
    PAGE_NUMBER_ILLEGAL(4006,"页码或者每页数量不符合规则"),

    //5000系列 角色错误
    ROLE_NOT_EXIST(5001,"角色ID不存在"),

    //6000系列 权限错误
    ACCESS_NOT_EXIST(6001,"权限ID不存在");


    //状态码
    private Integer code;
    //信息
    private String msg;

    ResultCode(Integer code, String msg){
        this.code = code;
        this.msg = msg;
    }
    public Integer getCode(){
        return code;
    }
    public void setCode(Integer code){
        this.code = code;
    }
    public String getMsg(){
        return msg;
    }
    public void setMsg(String msg){
        this.msg = msg;
    }
}
