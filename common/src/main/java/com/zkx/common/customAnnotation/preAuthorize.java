package com.zkx.common.customAnnotation;

import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface preAuthorize {
    String accessName() default "";
}
