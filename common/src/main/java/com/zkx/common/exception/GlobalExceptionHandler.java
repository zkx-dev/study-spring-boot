package com.zkx.common.exception;

import com.zkx.common.result.ResultVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

//全局异常处理类
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {
    /**
     * 自定义异常APIException
     * @param e
     * @return
     */
    @ExceptionHandler(APIException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResultVO APIExceptionHandler(APIException e){
        //状态码枚举类 作为自定义异常类的属性
        //拿到自定义异常类对象 获取对象中的枚举类属性 将枚举类中的code 和 msg 传给 ResultVO类作为属性。
        ResultVO resultVO;
        log.error(e.getResultCode().getMsg());
        resultVO = new ResultVO(e.getResultCode().getCode(),e.getResultCode().getMsg());
        return resultVO;

    }

}
