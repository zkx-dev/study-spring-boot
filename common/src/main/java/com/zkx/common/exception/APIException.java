package com.zkx.common.exception;

import com.zkx.common.result.ResultCode;
import lombok.Data;

//自定义异常类
@Data
public class APIException extends RuntimeException{
    //将响应码枚举类 设置为类的属性
    private final ResultCode resultCode;

    public APIException(ResultCode resultCode){
        this.resultCode = resultCode;
    }

    public ResultCode getResultCode(){
        return resultCode;
    }

}
