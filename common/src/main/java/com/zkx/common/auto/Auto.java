package com.zkx.common.auto;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

public class Auto {
    public static void main(String[] args) {
        //创建generator对象（代码生成器）
        AutoGenerator autoGenerator = new AutoGenerator();
        //数据源
        DataSourceConfig dataSourceConfig = new DataSourceConfig();
        dataSourceConfig.setDbType(DbType.MYSQL);
        dataSourceConfig.setUrl("jdbc:mysql://localhost:3306/challenge?characterEncoding=utf8&useSSL=false&serverTimezone=UTC&rewriteBatchedStatements=true&allowPublicKeyRetrieval=true");
        dataSourceConfig.setDriverName("com.mysql.cj.jdbc.Driver");
        dataSourceConfig.setUsername("root");
        dataSourceConfig.setPassword("123456");
        //把数据源装入generator对象
        autoGenerator.setDataSource(dataSourceConfig);

        //全局配置
        GlobalConfig globalConfig = new GlobalConfig();
        //设置文件生成路径
        globalConfig.setOutputDir(System.getProperty("user.dir")+"/src/main/java");
        //创建好之后不自动打开文件
        globalConfig.setOpen(false);
        //启动Swagger2
        globalConfig.setSwagger2(true);
        //将全局配置对象装入generator对象
        autoGenerator.setGlobalConfig(globalConfig);

        //自动生成文件需要放入的包信息
        PackageConfig packageConfig = new PackageConfig();
        //设置自动生成文件的包  放在哪个包下面
        packageConfig.setParent("com.zkx.system");
        packageConfig.setModuleName("generator");
        packageConfig.setController("controller");
        packageConfig.setService("service");
        packageConfig.setServiceImpl("service");
        packageConfig.setEntity("entity");

        autoGenerator.setPackageInfo(packageConfig);

        //配置策略
        StrategyConfig strategyConfig = new StrategyConfig();
        //实体类生成之后自动添加Lombok注解
        strategyConfig.setEntityLombokModel(true);
        //数据表字段的_命名 在生成实体类时变成驼峰式命名
        strategyConfig.setNaming(NamingStrategy.underline_to_camel);
        strategyConfig.setColumnNaming(NamingStrategy.underline_to_camel);

        autoGenerator.setStrategy(strategyConfig);

        autoGenerator.execute();
    }
}

