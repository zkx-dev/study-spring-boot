package com.zkx.system.config;

import com.zkx.system.interceptor.JWTInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class InterceptorConfig implements WebMvcConfigurer {
    //springboot拦截器在bean实例之前执行，Bean实例无法注入，拦截器中没有实例化redistemplate
    //需要在加入拦截器之前，先进行bean处理
    @Bean
    public JWTInterceptor jwtInterceptor(){
        return new JWTInterceptor();
    }

    /**
     * 添加拦截器
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //registry.addInterceptor(new JWTInterceptor()) 错误
        registry.addInterceptor(jwtInterceptor())
                .addPathPatterns("/record/borrow")
                .addPathPatterns("/record/return")
                .addPathPatterns("/record/selectRecordByToken")
                .addPathPatterns("/user/update")
                .addPathPatterns("/user/select")
                .addPathPatterns("/user/delete")
                .addPathPatterns("/user/out")
                //.addPathPatterns("/role/selectUandRByToken")
                //.addPathPatterns("/role/selectUandAByToken")
                .addPathPatterns("/record/selectAllRecord")
                .addPathPatterns("/access/delete")

        ;//其他接口token验证
    }
}
