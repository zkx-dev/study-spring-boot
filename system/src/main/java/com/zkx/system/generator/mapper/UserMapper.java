package com.zkx.system.generator.mapper;

import com.zkx.system.generator.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2021-08-10
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
    //注册
    @Insert("insert into user (user_name,password) values(#{userName},#{password})")
    void insertUser(User user);
    //修改
    @Update("update user set user_name = #{userName}, password = #{password} where id = #{id}")
    void updateUser(User user);
}
