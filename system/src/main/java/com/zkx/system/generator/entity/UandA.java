package com.zkx.system.generator.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
//存放查找的用户和权限的数据
public class UandA {
    private Integer userId;
    private String userName;
    private Integer accessId;
    private String function;
}
