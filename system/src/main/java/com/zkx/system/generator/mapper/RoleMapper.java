package com.zkx.system.generator.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zkx.system.generator.entity.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface RoleMapper extends BaseMapper<Role> {
    //根据角色id查看对应权限
    @Select("select ar.role_id,r.role_name,ar.access_id,a.function from access_role ar,access a,role r where ar.role_id = #{roleId} and r.id = #{roleId} and ar.access_id = a.id")
    List<RandA> selectFunctionByRoleId(Integer roleId);
    //根据用户id查看对应角色
    @Select("select ru.user_id,u.user_name,ru.role_id,r.role_name from user u,role r,role_user ru where ru.user_id = #{userId} and ru.user_id = u.id and ru.role_id = r.id")
    List<UandR> selectUandRByUserId(Integer userId);
    //根据用户id查看对应权限
    @Select("select ru.user_id,u.user_name,ar.access_id,a.function\n" +
            "from user u inner join\n" +
            "role_user ru inner join\n" +
            "access_role ar inner join\n" +
            "access a\n" +
            "where u.id = #{userId} and u.id = ru.user_id and ru.role_id = ar.role_id and ar.access_id = a.id;")
    List<UandA> selectUandAByUserId(Integer userId);
    //根据用户id查看对应角色 和对应权限
    @Select("select ru.user_id,u.user_name,ru.role_id,r.role_name,ar.access_id,a.function\n" +
            "from user u inner join\n" +
            "role_user ru inner join\n" +
            "access_role ar inner join\n" +
            "access a inner join\n" +
            "role r\n"+
            "where u.id = #{userId} and u.id = ru.user_id and ru.role_id = ar.role_id and ar.access_id = a.id and ru.role_id = r.id;")
    List<Msg> selectMsgByUserId(Integer userId);

}
