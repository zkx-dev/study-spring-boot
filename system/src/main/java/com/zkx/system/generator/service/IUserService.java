package com.zkx.system.generator.service;

import com.zkx.system.generator.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ${author}
 * @since 2021-08-10
 */
public interface IUserService extends IService<User> {

}
