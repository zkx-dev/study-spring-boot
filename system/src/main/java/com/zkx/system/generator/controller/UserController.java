package com.zkx.system.generator.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zkx.system.generator.entity.UandRandA;
import com.zkx.system.generator.entity.User;
import com.zkx.system.generator.service.UserServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import nonapi.io.github.classgraph.json.JSONUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2021-08-10
 */
@RestController
@RequestMapping("user")
@Api(tags = "用户管理，增删改查")
public class UserController {
    @Autowired
    UserServiceImpl userService;

    //添加新用户
    @ApiOperation(value = "新增用户",response = String.class)
    @PostMapping("insert")
    public String insert(@RequestBody User user){
        return userService.insert(user);
    }

    //用户登录
    @ApiOperation(value = "用户登录",response = String.class)
    @GetMapping("login")
    public String login(@RequestParam Integer id,@RequestParam String name,@RequestParam String password){
        return userService.login(id,name,password);
    }

    //指定用户更新信息
    @PutMapping("update")
    @ApiOperation(value = "更新指定用户信息",response = User.class)
    public User update(@RequestBody User user,HttpServletRequest request){
        return userService.update(user,request.getHeader("token"));
    }

    //删除指定用户
    @ApiOperation(value = "删除指定用户",response = User.class)
    @GetMapping("delete")
    public UandRandA delete(HttpServletRequest request){
        return userService.delete(request.getHeader("token"));
    }

    //查找指定用户
    @ApiOperation(value = "查找指定用户",response = User.class)
    @GetMapping("select")
    public UandRandA select(HttpServletRequest request){
        return userService.select(request.getHeader("token"));
    }

    //指定用户退出登录
    @ApiOperation(value = "指定用户退出登录",response = String.class)
    @GetMapping("out")
    public String out(HttpServletRequest request){
        return userService.out(request.getHeader("token"));
    }

    //分页查找所有用户
    @ApiOperation(value = "分页查找所有用户",response = Page.class)
    @GetMapping("selectAll")
    //pageNumber 请求的页码 number 每页的记录数
    public Page<User> selectAll(@RequestParam Integer pageNumber, @RequestParam Integer number){
        return userService.selectAll(pageNumber,number);
    }
}

