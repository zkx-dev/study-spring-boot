package com.zkx.system.generator.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zkx.common.exception.APIException;
import com.zkx.system.generator.entity.Book;
import com.zkx.system.generator.entity.Record;
import com.zkx.system.generator.mapper.BookMapper;
import com.zkx.system.generator.mapper.RecordMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zkx.common.result.ResultCode;
import com.zkx.common.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2021-08-10
 */
@Service
public class BookServiceImpl extends ServiceImpl<BookMapper, Book> implements IBookService {

    @Autowired
    BookMapper bookMapper;

    @Autowired
    RecordMapper recordMapper;

    @Autowired
    RedisUtil redisUtil;
    //RedisUtil redisUtil = ApplicationContextProvider.getBean(RedisUtil.class);

    public String insert(Book book){


        //图书名为"" 抛异常
        if(book.getBookName().equals("")){
            throw new APIException(ResultCode.BOOK_NAME_EMPTY);
        }
        //图书名长度不符合要求 抛异常
        else if(book.getBookName().length()<1||book.getBookName().length()>50){
            throw new APIException(ResultCode.BOOK_NAME_ILLEGAL);
        }
        //图书数量不符合要求 抛异常
        else if(book.getQuantity()<1||book.getQuantity()>10000000){
            throw new APIException(ResultCode.BOOK_QUANTITY_ILLEGAL);
        }
        //符合要求 向数据库添加新图书成功
        else{
            bookMapper.insertBook(book);
            return "insert success";
        }
    }

    public Book update(Book book){
        LinkedList<Record> recordList = recordMapper.selectRecordBookId(book.getId(),0);
        if(ifIdExist(book.getId())){
            //打算更新的图书有人借阅 无法更新图书信息
            if(recordList.size()!=0){
                throw new APIException(ResultCode.BOOK_HAS_BORROWED);
            }else{
                //图书名为"" 抛异常
                if(book.getBookName().equals("")){
                    throw new APIException(ResultCode.BOOK_NAME_EMPTY);
                }
                //图书名长度不符合要求 抛异常
                else if(book.getBookName().length()<1||book.getBookName().length()>50){
                    throw new APIException(ResultCode.BOOK_NAME_ILLEGAL);
                }
                //图书数量不符合要求 抛异常
                else if(book.getQuantity()<1||book.getQuantity()>10000000){
                    throw new APIException(ResultCode.BOOK_QUANTITY_ILLEGAL);
                }
                else{
                    bookMapper.updateBook(book);
                    return book;
                }
            }
        }
        else{
            throw new APIException(ResultCode.BOOK_NOT_EXIST);
        }
    }

    public Book delete(Integer id) {
        if(ifIdExist(id)){
            Book book = bookMapper.selectById(id);
            LinkedList<Record> recordList = recordMapper.selectRecordBookId(id,0);
            if(recordList.size()!=0){
                throw new APIException(ResultCode.BOOK_HAS_BORROWED);
            }
            else{
                bookMapper.deleteById(id);
                System.out.println("delete success");
                return book;
            }
        }
        else {
            throw new APIException(ResultCode.BOOK_NOT_EXIST);
        }
    }

    public Book select(Integer id){
        if(ifIdExist(id)){
            return bookMapper.selectById(id);
        }
        else{
            throw new APIException(ResultCode.BOOK_NOT_EXIST);
        }
    }

    //查找指定页码的所有图书信息
    //pageNumber 请求的页码 number 每页的记录数
    public Page<Book> selectAll(Integer pageNumber, Integer number){
        if(pageNumber<=0||number<1){
            throw new APIException(ResultCode.PAGE_NUMBER_ILLEGAL);
        }
        else{
            Page<Book> page = new Page<>(pageNumber,number);
            Page<Book> bookPage = bookMapper.selectPage(page,null);
            return bookPage;
        }
    }

    public boolean ifIdExist(Integer id){
        if(bookMapper.selectById(id)!=null){
            return true;
        }else{
            return false;
        }
    }
}
