package com.zkx.system.generator.entity;

import lombok.Data;

@Data
public class StaticClass {
    private static String token = "";

    public static void setToken(String token) {
        StaticClass.token = token;
    }

    public static String getToken() {
        return token;
    }
}
