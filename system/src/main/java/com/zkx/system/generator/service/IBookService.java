package com.zkx.system.generator.service;

import com.zkx.system.generator.entity.Book;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ${author}
 * @since 2021-08-10
 */
public interface IBookService extends IService<Book> {

}
