package com.zkx.system.generator.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zkx.common.customAnnotation.preAuthorize;
import com.zkx.system.generator.entity.Book;
import com.zkx.system.generator.service.BookServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2021-08-10
 */
@RestController
@RequestMapping("book")
@Api(tags = "图书管理，增删改查")
public class BookController {
    @Autowired
    BookServiceImpl bookService;


    @ApiOperation(value = "新增图书",response = String.class)
    @PostMapping("insert")
    public String insert(@RequestBody Book book){
        return bookService.insert(book);
    }


    @ApiOperation(value = "修改指定图书信息",response = Book.class)
    @PutMapping("update")
    public Book update(@RequestBody Book book){
        return bookService.update(book);
    }


    @ApiOperation(value = "删除指定图书",response = Book.class)
    @GetMapping("delete")
    public Book delete(@RequestParam Integer id){
        return bookService.delete(id);
    }

    @ApiOperation(value = "查看指定图书",response = Book.class)
    @GetMapping("select")
    public Book select(@RequestParam Integer id){
        return bookService.select(id);
    }

    @ApiOperation(value = "分页查找所有图书",response = Page.class)
    @GetMapping("selectAll")
    //pageNumber 请求的页码 number 每页的记录数
    public Page<Book> selectAll(@RequestParam Integer pageNumber, @RequestParam Integer number){
        return bookService.selectAll(pageNumber, number);
    }
}

