package com.zkx.system.generator.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zkx.common.customAnnotation.preAuthorize;
import com.zkx.system.generator.entity.RecordVO;
import com.zkx.system.generator.service.RecordServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2021-08-10
 */
@RestController
@RequestMapping("record")
@Api(tags = "借/还书")
public class RecordController {
    @Autowired
    RecordServiceImpl recordService;

    @preAuthorize(accessName = "borrow")
    @ApiOperation(value = "借书",response = String.class)
    @GetMapping("borrow")
    public String borrowBook(Integer bookId, HttpServletRequest request){
        return recordService.borrowOrReturnBook(bookId,true,request.getHeader("token"));
    }

    @preAuthorize(accessName = "return")
    @ApiOperation(value = "还书",response = String.class)
    @GetMapping("return")
    public String returnBook(Integer bookId, HttpServletRequest request){
        return recordService.borrowOrReturnBook(bookId,false,request.getHeader("token"));
    }

    @preAuthorize(accessName = "selectAllRecord")
    @ApiOperation(value = "分页查询所有借还书记录",response = Page.class)
    //分页查询所有借还书记录
    @GetMapping("selectAllRecord")
    public IPage<RecordVO> selectAllRecord(Integer pageNumber, Integer number,HttpServletRequest request){
        return recordService.selectAll(pageNumber, number,request.getHeader("token"));
    }

    @ApiOperation(value = "分页查询指定图书的借还书记录",response = Page.class)
    //分页查询指定图书的借还书记录
    @GetMapping("selectRecordByBookId")
    public IPage<RecordVO> selectRecordByBookId(Integer bookId,Integer pageNumber, Integer number){
        return recordService.selectRecordByBookId(bookId,pageNumber, number);
    }

    @ApiOperation(value = "分页查询指定用户的借还书记录",response = Page.class)
    //分页查询指定用户的借还书记录
    @GetMapping("selectRecordByToken")
    public IPage<RecordVO> selectRecordByToken(HttpServletRequest request,Integer pageNumber, Integer number){
        return recordService.selectRecordByToken(request.getHeader("token"),pageNumber, number);
    }

}

