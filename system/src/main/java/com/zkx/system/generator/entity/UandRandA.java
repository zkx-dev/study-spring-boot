package com.zkx.system.generator.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UandRandA implements Serializable {
    private Integer id;
    private String userName;
    private List<String> roles;
    private List<String> accesses;
    //private List<Msg> msgs;
}
