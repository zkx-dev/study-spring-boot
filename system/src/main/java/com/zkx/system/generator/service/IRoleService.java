package com.zkx.system.generator.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zkx.system.generator.entity.Role;


public interface IRoleService extends IService<Role> {
}
