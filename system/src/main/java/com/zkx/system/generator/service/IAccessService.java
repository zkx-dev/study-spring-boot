package com.zkx.system.generator.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zkx.system.generator.entity.Access;

public interface IAccessService extends IService<Access> {
}
