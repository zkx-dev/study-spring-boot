package com.zkx.system.generator.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
//存放查找的角色和权限的数据
public class RandA {
    private Integer roleId;
    private String roleName;
    private Integer accessId;
    private String function;
}
