package com.zkx.system.generator.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zkx.common.exception.APIException;
import com.zkx.system.generator.entity.*;
import com.zkx.system.generator.mapper.RoleMapper;
import com.zkx.common.result.ResultCode;
import com.zkx.common.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {
    @Autowired
    RoleMapper roleMapper;
    @Autowired
    RedisUtil redisUtil;
    //RedisUtil redisUtil = ApplicationContextProvider.getBean(RedisUtil.class);
    @Autowired
    UserServiceImpl userService;

    //根据角色id查询权限
    public List<Object> selectRandAByRoleId(Integer roleId){
        if(redisUtil.hasKey("roleId "+roleId)){
            return redisUtil.lGet("roleId "+roleId,0,-1);
        }
        else{
            List<RandA> list = roleMapper.selectFunctionByRoleId(roleId);
            if(list.size()!=0){
                for (RandA randA : list) {
                    redisUtil.lSet("roleId "+roleId, randA);
                }
                return redisUtil.lGet("roleId "+roleId,0,-1);
            }else{
                throw new APIException(ResultCode.ROLE_NOT_EXIST);
            }
        }
    }

    //根据用户id查询角色
    public List<Object> selectUandRByUserId(String token){
        if(!userService.ifTokenExist(token)){
            throw new APIException(ResultCode.USER_NOT_EXIST);
        }
        else{
            UandRandA u = (UandRandA) redisUtil.get(token);
            Integer userId = u.getId();
            if(redisUtil.hasKey("UandR "+userId)){
                return redisUtil.lGet("UandR "+userId,0,-1);
            }
            else{
                List<UandR> uandR = roleMapper.selectUandRByUserId(userId);
                if(uandR.size()!=0){
                    for (UandR r : uandR) {
                        redisUtil.lSet("UandR " + userId, r);
                    }
                    return redisUtil.lGet("UandR "+userId,0,-1);
                }
                else{
                    throw new APIException(ResultCode.USER_NOT_ROLE);
                }
            }
        }
    }

    //根据用户id查询权限
    public List<Object> selectUandAByUserId(String token){
        if(!userService.ifTokenExist(token)){
            throw new APIException(ResultCode.USER_NOT_EXIST);
        }else{
            UandRandA u = (UandRandA) redisUtil.get(token);
            Integer userId = u.getId();
            if(redisUtil.hasKey("UandA "+userId)){
                return redisUtil.lGet("UandA "+userId,0,-1);
            }
            else{
                List<UandA> uandA = roleMapper.selectUandAByUserId(userId);
                if(uandA.size()!=0){
                    for (UandA a : uandA) {
                        redisUtil.lSet("UandA " + userId, a);
                    }
                    return redisUtil.lGet("UandA "+userId,0,-1);
                }
                else{
                    throw new APIException(ResultCode.USER_NOT_ACCESS);
                }
            }
        }
    }
}
