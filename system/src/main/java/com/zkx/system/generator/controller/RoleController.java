package com.zkx.system.generator.controller;

import com.zkx.common.customAnnotation.Permission;
import com.zkx.common.customAnnotation.preAuthorize;
import com.zkx.system.generator.entity.RandA;
import com.zkx.system.generator.entity.UandR;
import com.zkx.system.generator.service.RoleServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("role")
@Api(tags = "角色管理")
public class RoleController {
    @Autowired
    RoleServiceImpl roleService;


    @ApiOperation(value = "查找指定角色拥有的权限",response = Object.class)
    @GetMapping("selectRandAByRoleId")
    public List<Object> selectRandAByRoleId(Integer roleId){
        return roleService.selectRandAByRoleId(roleId);
    }

    //@Permission(accessName = "borrow")
    @ApiOperation(value = "查找指定用户拥有的角色",response = Object.class)
    @GetMapping("selectUandRByToken")
    public List<Object> selectUandRByToken(HttpServletRequest request){
        return roleService.selectUandRByUserId(request.getHeader("token"));
    }

    //@Permission(accessName = "borrow")
    @ApiOperation(value = "查找指定用户拥有的权限",response = Object.class)
    @GetMapping("selectUandAByToken")
    public List<Object> selectUandAByToken(HttpServletRequest request){
        return roleService.selectUandAByUserId(request.getHeader("token"));
    }
}
