package com.zkx.system.generator.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zkx.system.generator.entity.Access;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface AccessMapper extends BaseMapper<Access> {
    @Select("select * from access where id = #{id}")
    Access selectAccessByAccessId(Integer id);

    @Select("select role_id from access_role where access_id = #{id}")
    List<Integer> selectAccessRoleIdByAccessId(Integer id);


    @Delete("delete from access_role where access_id = #{id}")
    void deleteByAccessId(Integer id);
}
