package com.zkx.system.generator.service;

import com.zkx.system.generator.entity.Book;
import com.zkx.system.generator.entity.W;
import com.zkx.system.generator.mapper.BookMapper;
import com.zkx.system.generator.mapper.RecordMapper;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

//消息队列消费者
@Component
@RabbitListener(queues = "hello")
public class HelloConsumer {

    @Autowired
    RecordMapper recordMapper;

    @Autowired
    BookMapper bookMapper;

    @RabbitHandler
    public void executeHello(W w){
        Book book = bookMapper.selectById(w.getBookId());
        //借还书记录为null时
        if(w.getRecord()==null){
            recordMapper.insertRecord(w.getUserId(), w.getBookId(),w.getStatus());
            book.setQuantity(book.getQuantity()-1);
            bookMapper.updateBook(book);
            test();
        }else if(w.getRecord().getStatus().equals(0)){
            recordMapper.updateRecordStatus(w.getUserId(), w.getBookId(),w.getStatus());
            book.setQuantity(book.getQuantity()+1);
            bookMapper.updateBook(book);
            test();
        }else{
            recordMapper.insertRecord(w.getUserId(), w.getBookId(),w.getStatus());
            book.setQuantity(book.getQuantity()-1);
            bookMapper.updateBook(book);
            test();
        }
    }
    public void test(){
        try{
            Thread.sleep(10000);
        }catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("10s");
    }
}
