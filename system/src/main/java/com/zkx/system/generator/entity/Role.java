package com.zkx.system.generator.entity;

import io.swagger.annotations.ApiModel;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="Role对象", description="")
public class Role {
    private Integer id;
    private String roleName;
}
