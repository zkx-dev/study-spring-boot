package com.zkx.system.generator.controller;

import com.zkx.system.generator.entity.Access;
import com.zkx.system.generator.service.AccessServiceImpl;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("access")
@Api(tags = "权限管理，删除指定权限")
public class AccessController {
    @Autowired
    AccessServiceImpl accessService;

    //根据权限id删除指定权限
    @GetMapping("delete")
    public String delAccessByAccessId(Integer id, HttpServletRequest request){
        return accessService.delAccessByAccessId(id,request.getHeader("token"));
    }
}
