package com.zkx.system.generator.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zkx.system.generator.entity.Record;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zkx.system.generator.entity.RecordVO;
import org.apache.ibatis.annotations.*;

import java.util.LinkedList;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2021-08-10
 */
@Mapper
public interface RecordMapper extends BaseMapper<Record> {
    @Insert("insert into record (user_id,book_id,status) values (#{userId},#{bookId},#{status})")
    void insertRecord(Integer userId, Integer bookId, Integer status);

    @Select("select * from record where user_id = #{userId} and book_id = #{bookId} order by id desc")
    LinkedList<Record> selectRecordUserIdAndBookId(Integer userId, Integer bookId);

    @Update("update record set status=#{status} where user_id = #{userId} and book_id = #{bookId}")
    void updateRecordStatus(Integer userId, Integer bookId, Integer status);

    //查找指定用户和借还书状态的 借书记录
    @Select("select * from record where user_id = #{userId} and status = #{status}")
    LinkedList<Record> selectRecordUserId(Integer userId, Integer status);

    //查找指定图书和借还书状态的 借书记录
    @Select("select * from record where book_id = #{bookId} and status = #{status}")
    LinkedList<Record> selectRecordBookId(Integer bookId, Integer status);

    //分页查看所有借还书记录
    @Select("select r.id,r.user_id,u.user_name,r.book_id,b.book_name,r.status from user u,book b,record r where r.user_id=u.id and r.book_id=b.id ")
    IPage<RecordVO> selectInfoPage(IPage<Record> page);

    //分页查看某本书的借还书记录
    @Select("select r.id,r.user_id,u.user_name,r.book_id,b.book_name,r.status from user u,book b,record r where r.book_id = #{bookId} and b.id = r.book_id and r.user_id = u.id")
    IPage<RecordVO> selectBookInfoPage(IPage<Record> page, @Param("bookId") Integer bookId);

    //分页查看指定用户的借还书记录
    @Select("select r.id,r.user_id,u.user_name,r.book_id,b.book_name,r.status from user u,book b,record r where r.user_id = #{userId} and u.id = r.user_id and r.book_id = b.id")
    IPage<RecordVO> selectUserInfoPage(IPage<Record> page, @Param("userId") Integer userId);
}
