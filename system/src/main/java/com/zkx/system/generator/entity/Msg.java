package com.zkx.system.generator.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Msg {
    private Integer userId;
    private String userName;
    private Integer roleId;
    private String roleName;
    private Integer accessId;
    private String function;

    @Override
    public boolean equals(Object o) {
        Msg m = (Msg) o;
        return roleId.equals(m.roleId);
    }

    @Override
    public int hashCode() {
        Integer a = roleId;
        return a.hashCode();
    }
}
