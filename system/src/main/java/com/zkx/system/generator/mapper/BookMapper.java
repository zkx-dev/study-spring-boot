package com.zkx.system.generator.mapper;

import com.zkx.system.generator.entity.Book;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2021-08-10
 */
@Mapper
public interface BookMapper extends BaseMapper<Book> {
    //注册
    @Insert("insert into book (book_name,quantity) values(#{bookName},#{quantity})")
    void insertBook(Book book);
    //修改
    @Update("update book set book_name = #{bookName}, quantity = #{quantity} where id = #{id}")
    void updateBook(Book book);
}
