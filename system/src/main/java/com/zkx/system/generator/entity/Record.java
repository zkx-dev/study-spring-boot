package com.zkx.system.generator.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

/**
 * <p>
 * 
 * </p>
 *
 * @author ${author}
 * @since 2021-08-10
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
  @EqualsAndHashCode(callSuper = false)
    @ApiModel(value="Record对象", description="")
public class Record implements Serializable {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "借还书记录号")
        @TableId(value = "id", type = IdType.AUTO)
      private Integer id;

      @ApiModelProperty(value = "借/还书人id")
      private Integer userId;

      @ApiModelProperty(value = "图书id")
      private Integer bookId;

      @ApiModelProperty(value = "图书状态 0 被借走 1 已归还")
      private Integer status;


}
