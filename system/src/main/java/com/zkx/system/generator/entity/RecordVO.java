package com.zkx.system.generator.entity;

import io.swagger.annotations.ApiModel;
import lombok.*;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="RecordVO对象", description="")
public class RecordVO implements Serializable {
    private Integer id;
    private Integer userId;
    private String userName;
    private Integer bookId;
    private String bookName;
    private Integer status;
}
