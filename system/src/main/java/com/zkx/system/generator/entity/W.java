package com.zkx.system.generator.entity;

import lombok.Data;

import java.io.Serializable;


//借还书业务 使用消息队列的异步方法 定义一个类 将需要传入的数据作为类的属性
@Data
public class W implements Serializable {
    private Record record;
    private Integer userId;
    private Integer bookId;
    private Integer status;

    public W(Record record, Integer userId, Integer bookId, Integer status) {
        this.record = record;
        this.userId = userId;
        this.bookId = bookId;
        this.status = status;
    }
}
