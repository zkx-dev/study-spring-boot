package com.zkx.system.generator.entity;

import io.swagger.annotations.ApiModel;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="Access对象", description="")
public class Access {
    private Integer id;
    private String function;
}
