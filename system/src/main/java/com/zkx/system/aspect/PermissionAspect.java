package com.zkx.system.aspect;

import com.zkx.common.exception.APIException;
import com.zkx.system.generator.entity.UandRandA;
import com.zkx.common.result.ResultCode;
import com.zkx.common.utils.JWTUtils;
import com.zkx.common.utils.RedisUtil;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Configuration
@Aspect
@ResponseBody
public class PermissionAspect {

    @Autowired
    RedisUtil redisUtil;
    //RoleController类中 任何以ByToken结尾的方法
    @Pointcut("execution(* com.zkx.system.generator.controller.RoleController.*ByToken(..))")
    public void anyMethod(){}
//    @Before(value = "@annotation(p)")
//    public void PermissionCheck(JoinPoint joinPoint, Permission p){
//        UandRandA uandRandA;
//        String accessName;
//        Object[] objects = joinPoint.getArgs();
//        for (Object obj : objects) {
//            if(obj instanceof HttpServletRequest){
//                String token = ((HttpServletRequest) obj).getHeader("token");
//                if(token == null || token.equals("")){
//                    throw new APIException(ResultCode.TOKEN_EMPTY);
//                }
//                else{
//                    String answer = JWTUtils.getTokenInfo(token);
//                    if(answer.equals(token)&&redisUtil.hasKey(token)){
//                        accessName = p.accessName();
//                        uandRandA =  (UandRandA) redisUtil.get(token);
//                        if(uandRandA.getAccesses()!=null){
//                            if(!uandRandA.getAccesses().contains(accessName)){
//                                throw new APIException(ResultCode.NO_PERMISSION);
//                            }
//                        }
//                        else{
//                            throw new APIException(ResultCode.NO_PERMISSION);
//                        }
//                    }
//                    else{
//                        throw new APIException(ResultCode.TOKEN_ERROR);
//                    }
//                }
//            }
//            else{
//                //方法参数中没有HttpServletRequest类型的数据 抛异常
//                throw new APIException(ResultCode.VALIDATE_FAILED);
//            }
//        }
//    }
    @Before("anyMethod()")
    public void PermissionCheck(JoinPoint joinPoint){
        UandRandA uandRandA;
        String accessName;
        Object[] objects = joinPoint.getArgs();
        for (Object obj : objects) {
            if(obj instanceof HttpServletRequest){
                String token = ((HttpServletRequest) obj).getHeader("token");
                if(token == null || token.equals("")){
                    throw new APIException(ResultCode.TOKEN_EMPTY);
                }
                else{
                    String answer = JWTUtils.getTokenInfo(token);
                    if(answer.equals(token)&&redisUtil.hasKey(token)){
                        accessName = "borrow";
                        uandRandA =  (UandRandA) redisUtil.get(token);
                        if(uandRandA.getAccesses()!=null){
                            if(!uandRandA.getAccesses().contains(accessName)){
                                throw new APIException(ResultCode.NO_PERMISSION);
                            }
                        }
                        else{
                            throw new APIException(ResultCode.NO_PERMISSION);
                        }
                    }
                    else{
                        throw new APIException(ResultCode.TOKEN_ERROR);
                    }
                }
            }
            else{
                //方法参数中没有HttpServletRequest类型的数据 抛异常
                throw new APIException(ResultCode.VALIDATE_FAILED);
            }
        }
    }
}
