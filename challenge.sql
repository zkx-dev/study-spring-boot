/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 80023
 Source Host           : localhost:3306
 Source Schema         : challenge

 Target Server Type    : MySQL
 Target Server Version : 80023
 File Encoding         : 65001

 Date: 17/09/2022 14:40:29
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for access
-- ----------------------------
DROP TABLE IF EXISTS `access`;
CREATE TABLE `access`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '权限id',
  `function` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限内容',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of access
-- ----------------------------
INSERT INTO `access` VALUES (2, 'return');
INSERT INTO `access` VALUES (3, 'selectAllRecord');
INSERT INTO `access` VALUES (24, 'borrow');

-- ----------------------------
-- Table structure for access_role
-- ----------------------------
DROP TABLE IF EXISTS `access_role`;
CREATE TABLE `access_role`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '角色对应权限id',
  `role_id` int(0) NULL DEFAULT NULL COMMENT '角色id',
  `access_id` int(0) NULL DEFAULT NULL COMMENT '权限id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of access_role
-- ----------------------------
INSERT INTO `access_role` VALUES (2, 1, 2);
INSERT INTO `access_role` VALUES (3, 2, 3);
INSERT INTO `access_role` VALUES (19, 1, 24);

-- ----------------------------
-- Table structure for book
-- ----------------------------
DROP TABLE IF EXISTS `book`;
CREATE TABLE `book`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `book_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `quantity` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of book
-- ----------------------------
INSERT INTO `book` VALUES (1, '红楼梦', 10);
INSERT INTO `book` VALUES (2, '水浒传', 20);
INSERT INTO `book` VALUES (3, '西游记', 30);
INSERT INTO `book` VALUES (4, '三国演义', 40);
INSERT INTO `book` VALUES (5, '哆啦A梦', 10);
INSERT INTO `book` VALUES (6, 'PHP', 1);
INSERT INTO `book` VALUES (7, 'Python', 10);

-- ----------------------------
-- Table structure for record
-- ----------------------------
DROP TABLE IF EXISTS `record`;
CREATE TABLE `record`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '借还书记录号',
  `user_id` int(0) NULL DEFAULT NULL COMMENT '借/还书人id',
  `book_id` int(0) NULL DEFAULT NULL COMMENT '图书id',
  `status` int(0) NULL DEFAULT NULL COMMENT '图书状态 0 被借走 1 已归还',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 44 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of record
-- ----------------------------
INSERT INTO `record` VALUES (1, 1, 1, 1);
INSERT INTO `record` VALUES (2, 1, 2, 1);
INSERT INTO `record` VALUES (3, 1, 1, 1);
INSERT INTO `record` VALUES (4, 1, 6, 1);
INSERT INTO `record` VALUES (5, 2, 3, 1);
INSERT INTO `record` VALUES (6, 7, 7, 1);
INSERT INTO `record` VALUES (7, 7, 6, 1);
INSERT INTO `record` VALUES (8, 7, 5, 1);
INSERT INTO `record` VALUES (9, 7, 1, 1);
INSERT INTO `record` VALUES (13, 7, 1, 1);
INSERT INTO `record` VALUES (14, 6, 1, 1);
INSERT INTO `record` VALUES (15, 6, 2, 1);
INSERT INTO `record` VALUES (16, 6, 3, 1);
INSERT INTO `record` VALUES (17, 3, 1, 1);
INSERT INTO `record` VALUES (18, 3, 6, 1);
INSERT INTO `record` VALUES (19, 3, 4, 1);
INSERT INTO `record` VALUES (20, 1, 4, 1);
INSERT INTO `record` VALUES (21, 2, 4, 1);
INSERT INTO `record` VALUES (22, 1, 5, 1);
INSERT INTO `record` VALUES (24, 5, 1, 1);
INSERT INTO `record` VALUES (25, 1, 7, 1);
INSERT INTO `record` VALUES (26, 1, 6, 1);
INSERT INTO `record` VALUES (27, 1, 6, 1);
INSERT INTO `record` VALUES (28, 8, 2, 1);
INSERT INTO `record` VALUES (29, 9, 2, 1);
INSERT INTO `record` VALUES (30, 1, 3, 1);
INSERT INTO `record` VALUES (31, 1, 3, 1);
INSERT INTO `record` VALUES (32, 1, 3, 1);
INSERT INTO `record` VALUES (33, 1, 3, 1);
INSERT INTO `record` VALUES (34, 1, 6, 1);
INSERT INTO `record` VALUES (35, 1, 6, 1);
INSERT INTO `record` VALUES (36, 1, 7, 1);
INSERT INTO `record` VALUES (37, 1, 7, 1);
INSERT INTO `record` VALUES (38, 2, 3, 1);
INSERT INTO `record` VALUES (39, 2, 3, 1);
INSERT INTO `record` VALUES (40, 2, 3, 1);
INSERT INTO `record` VALUES (41, 2, 4, 1);
INSERT INTO `record` VALUES (42, 2, 4, 1);
INSERT INTO `record` VALUES (43, 2, 5, 1);
INSERT INTO `record` VALUES (44, 1, 5, 1);

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '角色id',
  `role_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色类型',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES (1, 'user');
INSERT INTO `role` VALUES (2, 'admin');

-- ----------------------------
-- Table structure for role_user
-- ----------------------------
DROP TABLE IF EXISTS `role_user`;
CREATE TABLE `role_user`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '用户对应角色id',
  `user_id` int(0) NULL DEFAULT NULL COMMENT '用户id',
  `role_id` int(0) NULL DEFAULT NULL COMMENT '角色id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role_user
-- ----------------------------
INSERT INTO `role_user` VALUES (1, 1, 1);
INSERT INTO `role_user` VALUES (2, 1, 2);
INSERT INTO `role_user` VALUES (3, 2, 2);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'wuhu', '111111');
INSERT INTO `user` VALUES (2, 'jerry', '111111');
INSERT INTO `user` VALUES (3, 'jack', '888888');
INSERT INTO `user` VALUES (4, 'lucy', '111111');
INSERT INTO `user` VALUES (5, 'xiaohei', '999999');
INSERT INTO `user` VALUES (6, 'maria', '987600');
INSERT INTO `user` VALUES (7, 'xixi', '563548');
INSERT INTO `user` VALUES (10, 'dasima', '000000');
INSERT INTO `user` VALUES (11, 'hanjinlong', '000000');

SET FOREIGN_KEY_CHECKS = 1;
