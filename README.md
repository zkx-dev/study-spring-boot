数据库，redis等配置文件位置
bookcontroll/common/src/main/resources/application.properties

启动目录
bookcontroller/system/src/main/java/com.zkx.system/SystemApplication

功能：
用户/管理员注册，登录，退出，注销
用户借书，还书
用户查看自己的借还书记录
管理员查看所有/指定用户借还书记录
管理员增删改查图书
管理员增删改查用户的权限
用户，管理员的权限控制


使用技术
jwt认证与鉴权（自定义拦截器查看token中用户的权限）
mybaits
redis
rabbitmq
springboot
代码文件模块化处理，分为common 和 system
自动生成数据库表对应的java类
统一异常处理，统一返回体控制
swagger测试